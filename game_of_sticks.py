
def game_rule():
    """виводить правила гри"""
    print("""Грають два гравці. Є певна кількість палочок. 
Гравці берутом від однієї до трьох палочок почерзі.
Грають до тех пір, поки незакінчаться палички. 
Той хто взяв останю - той програв.""")
    game()


def game_player(number_player: str):
    """Вертає кількість паличок яких гравець забирає"""
    print(number_player, "Забиріть 1, 2, або 3 палочки")
    player_move = int(input("введіть кількість забраних паличок "))
    return player_move


def game():
    """Запрошує гравців ввести свої іменаб
Вводитться кількість паличок в гріб2забрав останню паличку
"""
    player1 = input('enter name player 1 ')
    player2 = input('enter name player 2 ')
    sticks = int(input('Введіть кількість паличок '))
    while True:
        print('Кількість паличок в грі \n', 'I ' * sticks)
        sticks -= game_player(player1)
        if sticks < 1:
            print(player1, "нажаль ви програли")
            break
        print('Кількість паличок в грі \n', 'I ' * sticks)
        sticks -= game_player(player2)
        if sticks < 1:
            print(player2, "нажаль ви програли")
            break


def main():
    """Виводить правила гри при введенні 1,
Розпочинає гру при введенні 2 """
    print('game of sticks')
    print('''Розпочинаємо гру.
для перегляду правил гри натисніть 1,
для початку гри натисніть 2''')
    you_choice = int(input("зробить свій вибір "))
    if you_choice == 1:
        game_rule()
    elif you_choice == 2:
        game()
    print("GAME OVER")


if __name__ == "__main__":
    main()
